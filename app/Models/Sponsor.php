<?php
namespace App\Models;
use Core\App;
class Sponsor {
	public static function getAllSponsors()
	{	
		return App::get('database')->select('sponsors');
	}

	public static function create($fields)
	{	
		return App::get('database')->insert('sponsors', $fields);
	}
}